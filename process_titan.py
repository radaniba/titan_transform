from __future__ import division
import pandas as pd
import argparse
import yaml

parser = argparse.ArgumentParser()
parser.add_argument('--config', help='''config file containing subcases and calls type''')
parser.add_argument('--output', help='''output file''')
args = parser.parse_args()


def get_subtypes(config_file):
    """
    :param config_file: yaml config file
    :rtype : dictionary of the possible subtypes

    """
    subtypes = {}
    for key, value in yaml.load(open(config_file))['subtypes'].iteritems():
        subtypes[key] = value
    return subtypes


def map_titan_calls(config_file):
    """
    :param config_file: yaml config file
    :rtype : dictionnary of possible mapping of titan calls
    """
    call_map = {}
    for key, value in yaml.load(open(config_file))['calls'].iteritems():
        call_map[key] = value
    return call_map


def read_titan_file(config_file, file_name, subtype, output_file):
    """
    :param config_file: yaml config file
    :param file_name: file name of the subtype titan
    :param subtype: label of the subtype
    :param output_file: output file
    :rtype : nothing, this function is just creating a file
    """
    call_merge = map_titan_calls(config_file)
    titan_file = pd.read_csv(file_name, sep="\t")
    # sum the total length of chromosomes
    total_length = titan_file.groupby(by=['Sample'])['Length(bp)'].sum()
    # add another column with the fraction
    fractions = []
    for index, row in titan_file.iterrows():
        fractions.append(row['Length(bp)'] / total_length[row['Sample']])
    titan_file['fraction'] = fractions
    # titan_file['fraction'] = [np.divide(float(item),float(value)) for
    # item in titan_file['Length(bp)']
    titan_file['Subtype'] = subtype
    result = pd.DataFrame()
    result['Subtype'] = titan_file['Subtype']
    result['titan_call'] = [call_merge[item]
                            for item in titan_file['TITAN_call']]
    result['Sample'] = titan_file['Sample']
    result['fraction'] = titan_file['fraction']
    with open(output_file, 'a') as f:
        result.to_csv(f, sep="\t", header=False, index=False)


def main(args):
    ff_subtypes = get_subtypes(args.config)
    for key, value in ff_subtypes.iteritems():
        read_titan_file(args.config, value, key, args.output)


main(args)
